## README ##

An example RESTful API that allows creating, shuffling and deleting a deck of 52 playing cards.

### Build and test project ###
$ gradle build

### Start the server ###

$ java -jar build/libs/shuffler-0.0.1-SNAPSHOT.jar

OR

$ gradle bootRun

### Start the server with custom shuffler ###
$ java -DshufflerClass=com.example.SimpleShuffler -jar build/libs/shuffler-0.0.1-SNAPSHOT.jar

OR

$ gradle bootRun -PjvmArgs="-DshufflerClass=com.example.SimpleShuffler"


### View all decks ###
$ curl -X GET http://localhost:8080/deck/v0/

[]

### Create deck(s) and view them (idempotent create) ###
$ curl -X PUT http://localhost:8080/deck/v0/create/a

{"name":"a","cards":"2-clubs,3-clubs,...,K-spades,A-spades"}

$ curl -X PUT http://localhost:8080/deck/v0/create/b

{"name":"b","cards":"2-clubs,3-clubs,...,K-spades,A-spades"}

$ curl -X PUT http://localhost:8080/deck/v0/create/b

{"name":"b","cards":"2-clubs,3-clubs,...,K-spades,A-spades"}

$ curl -X GET http://localhost:8080/deck/v0/

[{"name":"a","cards":"2-clubs,3-clubs,...,K-spades,A-spades"},{"name":"b","cards":"2-clubs,3-clubs,...,K-spades,A-spades"}]

### Get the deck's card ordering ###
$ curl -X GET http://localhost:8080/deck/v0/a

2-clubs,3-clubs,4-clubs,5-clubs,6-clubs,7-clubs,8-clubs,9-clubs,10-clubs,J-clubs,Q-clubs,K-clubs,A-clubs,2-diamonds,3-diamonds,4-diamonds,5-diamonds,6-diamonds,7-diamonds,8-diamonds,9-diamonds,10-diamonds,J-diamonds,Q-diamonds,K-diamonds,A-diamonds,2-hearts,3-hearts,4-hearts,5-hearts,6-hearts,7-hearts,8-hearts,9-hearts,10-hearts,J-hearts,Q-hearts,K-hearts,A-hearts,2-spades,3-spades,4-spades,5-spades,6-spades,7-spades,8-spades,9-spades,10-spades,J-spades,Q-spades,K-spades,A-spades

### Get deck that doesn't exist ###

$ curl -X GET http://localhost:8080/deck/v0/c

{"timestamp":1449044246244,"status":404,"error":"Not Found","exception":"com.example.DeckNotFoundException","message":"No message available","path":"/deck/v0/c"}

### Shuffle a deck ###
$ curl -X POST http://localhost:8080/deck/v0/shuffle/a

{"name":"a","cards":"4-diamonds,5-clubs,...,8-diamonds,2-diamonds"}

$ curl -X GET http://localhost:8080/deck/v0/a

4-diamonds,5-clubs,7-spades,8-spades,Q-diamonds,J-spades,J-hearts,A-diamonds,6-clubs,2-spades,K-spades,9-clubs,Q-spades,3-spades,2-clubs,5-spades,J-diamonds,Q-clubs,A-spades,K-clubs,A-hearts,10-spades,J-clubs,K-hearts,5-diamonds,10-diamonds,Q-hearts,4-hearts,4-spades,4-clubs,5-hearts,7-clubs,8-hearts,10-hearts,7-diamonds,K-diamonds,7-hearts,A-clubs,9-spades,6-hearts,8-clubs,3-diamonds,6-diamonds,2-hearts,6-spades,9-hearts,3-hearts,9-diamonds,10-clubs,3-clubs,8-diamonds,2-diamonds

### Delete a deck ###
$ curl -X DELETE http://localhost:8080/deck/v0/delete/b

true

$ curl -X DELETE http://localhost:8080/deck/v0/delete/b

false

$ curl -X GET http://localhost:8080/deck/v0

[{"name":"a","cards":"4-diamonds,5-clubs,...,8-diamonds,2-diamonds"}]


#Scope Reductions#
1) No database. Decks are stored in memory and are only available while the server is up.
When the server restarts, all decks are deleted.  In a real project, we'd have some kind of permanent storage.

2) No pure JSON API. Ties in with #2 as there is no response object returned from each request, so responses are a mix of text and JSON.
A real project would return responses that are always valid JSON objects which would include content as well as errors if any occurred.

3) Not thread safe. A real project would lock access to the decks when modifying them.

4) Deploy time configuration. The shuffler can be changed if the server is restarted so this isn't runtime configurable as the requirements
state.

5) Other scope reductions are listed in the code under //TODO: tags. 