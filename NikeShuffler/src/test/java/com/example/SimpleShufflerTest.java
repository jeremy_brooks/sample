package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleShufflerTest {

	@Test
	public void testShuffle() {
		Deck deck = Deck.newBuilder().create("myDeck");
		Shuffler shuffler = new SimpleShuffler();
		deck.shuffle(shuffler);
		String shuffledOrdering = deck.getCurrentOrdering();
		assertNotEquals(DeckTest.INITIAL_ORDERING, shuffledOrdering); 
		//TODO: for scope reduction: we assume a valid shuffling is not the initial ordering
		//(but in reality it is).
	}

}
