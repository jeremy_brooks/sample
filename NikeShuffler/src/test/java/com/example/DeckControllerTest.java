package com.example;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class DeckControllerTest {

	private MockMvc mvc;
	private DeckController controller;
	private DeckRepository repo;
	
	@Before
	public void setUp() throws Exception {
		repo = Mockito.mock(DeckRepository.class);
		controller = new DeckController();
		controller.setRepo(repo);
		controller.setShuffler(new SimpleShuffler());
		
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void shouldGetNothing() throws Exception {
		Mockito.when(repo.findAll()).thenReturn(Collections.emptySet());
		
		mvc.perform(MockMvcRequestBuilders.get("/deck/v0/").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(equalTo("[]")));
	}

	@Test
	public void shouldGetAllDecks() throws Exception {
		Set<Deck> deckSet = new HashSet<>();
		Deck deck1 = Deck.newBuilder().create("jeremysDeck");
		deckSet.add(deck1);
		Mockito.when(repo.findAll()).thenReturn(deckSet);
		
		mvc.perform(MockMvcRequestBuilders.get("/deck/v0/").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(equalTo("[{\"name\":\"jeremysDeck\",\"cards\":\"2-clubs,3-clubs,...,K-spades,A-spades\"}]")));
	}

	@Test
	public void shouldThrowOnGetNonExistingDeck() throws Exception {
		Mockito.when(repo.findByName(Matchers.anyObject())).thenReturn(null);

		mvc.perform(MockMvcRequestBuilders.get("/deck/v0/doesntExistDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(HttpStatus.NOT_FOUND.value()));
	}

	@Test
	public void shouldGetCardOrdering() throws Exception {
		Set<Deck> deckSet = new HashSet<>();
		Deck deck1 = Deck.newBuilder().create("jeremysDeck");
		deckSet.add(deck1);
		Mockito.when(repo.findByName(Matchers.anyString())).thenReturn(deck1);
		
		mvc.perform(MockMvcRequestBuilders.get("/deck/v0/jeremysDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(equalTo(DeckTest.INITIAL_ORDERING)));
	}

	@Test
	public void shouldAddNewDeck() throws Exception {
		Set<Deck> deckSet = new HashSet<>();
		Deck deck1 = Deck.newBuilder().create("anotherDeck");
		deckSet.add(deck1);
		Mockito.when(repo.add(Matchers.anyObject())).thenReturn(deck1);

		mvc.perform(MockMvcRequestBuilders.put("/deck/v0/create/anotherDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(content().string(equalTo("{\"name\":\"anotherDeck\",\"cards\":\"2-clubs,3-clubs,...,K-spades,A-spades\"}")));
	}

	@Test
	public void shouldDeleteDeck() throws Exception {
		Set<Deck> deckSet = new HashSet<>();
		Deck deck1 = Deck.newBuilder().create("anotherDeck");
		deckSet.add(deck1);
		Mockito.when(repo.delete(Matchers.anyObject())).thenReturn(true);

		mvc.perform(MockMvcRequestBuilders.delete("/deck/v0/delete/anotherDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().string(equalTo("true")));
	}

	@Test
	public void shouldNotDeleteDeck() throws Exception {
		Mockito.when(repo.delete(Matchers.anyObject())).thenReturn(false);

		mvc.perform(MockMvcRequestBuilders.delete("/deck/v0/delete/doesntExistDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().string(equalTo("false")));
	}

	@Test
	public void shouldShuffleDeck() throws Exception {
		Set<Deck> deckSet = new HashSet<>();
		Deck deck1 = Deck.newBuilder().create("anotherDeck");
		deckSet.add(deck1);
		Mockito.when(repo.findByName(Matchers.anyObject())).thenReturn(deck1);

		mvc.perform(MockMvcRequestBuilders.post("/deck/v0/shuffle/anotherDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
	}

	@Test
	public void shouldThrowOnShuffleDeck() throws Exception {
		Mockito.when(repo.findByName(Matchers.anyObject())).thenReturn(null);

		mvc.perform(MockMvcRequestBuilders.post("/deck/v0/shuffle/anotherDeck").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(HttpStatus.NOT_FOUND.value()));
	}

}