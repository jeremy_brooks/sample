package com.example;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class DeckTest {

	static final String INITIAL_ORDERING = "2-clubs,3-clubs,4-clubs,5-clubs,6-clubs,7-clubs,8-clubs,9-clubs,10-clubs,J-clubs,Q-clubs,K-clubs,A-clubs,2-diamonds,3-diamonds,4-diamonds,5-diamonds,6-diamonds,7-diamonds,8-diamonds,9-diamonds,10-diamonds,J-diamonds,Q-diamonds,K-diamonds,A-diamonds,2-hearts,3-hearts,4-hearts,5-hearts,6-hearts,7-hearts,8-hearts,9-hearts,10-hearts,J-hearts,Q-hearts,K-hearts,A-hearts,2-spades,3-spades,4-spades,5-spades,6-spades,7-spades,8-spades,9-spades,10-spades,J-spades,Q-spades,K-spades,A-spades";
	static final String INITIAL_SHORT_ORDERING = "2-clubs,3-clubs,...,K-spades,A-spades";

	@Test
	public void givenNewDeck()
	{
		Deck deck = Deck.newBuilder().create("one");
		assertEquals("Deck [name=one, " + INITIAL_ORDERING + "]",deck.toString());
		assertEquals("one", deck.getName());
		assertNotNull(deck.getCards());
		assertEquals(INITIAL_SHORT_ORDERING, deck.getCards());
		String actualOrdering = deck.getCurrentOrdering();
		assertEquals(INITIAL_ORDERING, actualOrdering);
		System.out.println(deck.toString());
	}

	
	@Test
	public void givenTwoNamedDecks()
	{
		Deck deck = Deck.newBuilder().create("one");
		Deck deck2 = Deck.newBuilder().create("two");
		assertNotSame(deck, deck2);
		assertFalse(deck.equals(deck2));
		assertEquals("one", deck.getName());
		assertEquals("two", deck2.getName());
	}
}
