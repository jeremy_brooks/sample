package com.example;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DeckRepository {
	private Map<String, Deck> deckMap = new HashMap<>();

	public Set<Deck> findAll() {
		Set<Deck> set = new HashSet<>();
		for(Deck d: deckMap.values())
		{
			set.add(d);
		}
		return set;
	}

	public Deck findByName(String name) {
		return deckMap.get(name);
	}

	public Deck add(Deck deck) {
		String name = deck.getName();
		deckMap.put(name, deck);
		return findByName(name);
	}

	public boolean delete(String name) {
		Deck removed = deckMap.remove(name);
		return removed != null ? true : false;
	}

}
