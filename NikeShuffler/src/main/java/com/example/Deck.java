package com.example;

import java.util.ArrayList;
import java.util.List;

public class Deck {
	private String name;
	private List<String> cards = new ArrayList<>(52);

	public Deck(String name, List<String> cards) {
		this.name = name;
		this.cards = cards;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Deck [name=" + name + ", ");//, cards=" + cards + "]";
		for(String card: cards)
			sb.append(card).append(",");
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deck other = (Deck) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public String getCards() {
		StringBuilder sb = new StringBuilder();
		int[] indexes = new int[]{0,1};
		for(int index: indexes)
		{
			sb.append(cards.get(index)).append(",");
		}
		sb.append("...,");
		indexes = new int[]{50,51};
		for(int index: indexes)
		{
			sb.append(cards.get(index)).append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public void shuffle(Shuffler shuffler){
		if(shuffler == null) throw new NullPointerException("shuffler arg cannot be null");
		
		shuffler.shuffle(cards);
	}

	/*package scope for testing*/
	String getCurrentOrdering() {
		StringBuilder sb = new StringBuilder();
		for(String card: cards)
		{
			sb.append(card).append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}


	public static Deck.Builder newBuilder() {
		return new Deck.Builder();
	}

	
	public static class Builder {
		private List<String> cards = new ArrayList<>(52);
		
		private static String[] suits = {"clubs","diamonds","hearts","spades"};
		private static String[] ranks = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
		
		public Deck create(String name)
		{
			for(String suit: suits)
				for (String rank: ranks)
					cards.add(rank + "-" + suit);
			return new Deck(name, cards);
		}
	}

}
