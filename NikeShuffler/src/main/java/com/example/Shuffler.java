package com.example;

import java.util.List;

public interface Shuffler {
	
	/**
	 * Shuffles the given deck of cards
	 * @param cards the cards to be shuffled in-place
	 */
	void shuffle(List<String> cards);
}
