package com.example;

import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deck/v0")
public class DeckController {
	private static Logger log = Logger.getLogger(DeckController.class);
	private DeckRepository repo = new DeckRepository();
	private Shuffler shuffler;
	
	@PostConstruct
	public void init()
	{
		String shufflerClassName = System.getProperty("shufflerClass");
		if(shufflerClassName == null || shufflerClassName.isEmpty())
			shufflerClassName = "com.example.SimpleShuffler";
		
		initShuffler(shufflerClassName);
	}

	private void initShuffler(String className) {
		try {
	    	  log.info(String.format("using shuffler: %s", className));
	    	  Class<?> cls = Class.forName(className);
	    	  setShuffler((Shuffler) cls.newInstance());
	      } catch (ClassNotFoundException e) {
	    	  log.error("shuffler class not found", e);
	    	  throw new RuntimeException(e);
	      } catch (InstantiationException e) {
	    	  log.error("unable to instantiate shuffler", e);
	    	  throw new RuntimeException(e);
	      } catch (IllegalAccessException e) {
	    	  log.error("illegal access", e);
	    	  throw new RuntimeException(e);
	      }
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public Set<Deck> getAll() {
        log.info(String.format("get all decks"));
        return repo.findAll();
    }

    @RequestMapping(method=RequestMethod.GET, value="{name}")
    public @ResponseBody String getDeck(@PathVariable String name) {
        Deck deck = repo.findByName(name);
        
        if(deck != null){
            log.info(String.format("get deck cards: %s", name));
        	return deck.getCurrentOrdering();
        } else {
            log.info(String.format("deck not found: %s", name));
        	throw new DeckNotFoundException();
        }
    }

    @RequestMapping(method=RequestMethod.PUT, value="create/{name}", produces="application/json")
    public @ResponseBody Deck add(@PathVariable String name) {
      Deck deck = repo.findByName(name);
      if(deck != null) return deck;

      log.info(String.format("create deck: %s", name));
      deck = Deck.newBuilder().create(name);
      return repo.add(deck);
    }

    @RequestMapping(method=RequestMethod.DELETE, value="delete/{name}")
    public @ResponseBody boolean delete(@PathVariable String name) {
    	log.info(String.format("delete deck: %s", name));
    	return repo.delete(name);
    }

    @RequestMapping(method=RequestMethod.POST, value="shuffle/{name}", produces="application/json")
    public @ResponseBody Deck shuffle(@PathVariable String name) {
      Deck deck = repo.findByName(name);
      if(deck == null){
          log.info(String.format("deck not found: %s", name));
    	  throw new DeckNotFoundException();
      }
  	  log.info(String.format("shuffle deck: %s", name));
      deck.shuffle(shuffler);
      return deck;
    }

//    @RequestMapping(method=RequestMethod.PUT, value="updateShuffler/{className}")
//    public @ResponseBody String updateShuffler(@PathVariable String className) {
//    	//TODO: scope reduction: shuffler class must exist in the following package
//    	initShuffler("com.example." + className);
//    	return className;
//    }

	public void setRepo(DeckRepository repo) {
		this.repo = repo;
	}

	public void setShuffler(Shuffler shuffler) {
		this.shuffler = shuffler;
	}
    
	
    
}