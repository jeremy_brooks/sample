package com.example;

import java.util.Collections;
import java.util.List;

public class SimpleShuffler implements Shuffler {

	@Override
	public void shuffle(List<String> cards) {
		Collections.shuffle(cards);
	}

}
